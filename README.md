# Cambrian Explosion
### Goal
> To provide a language for users of Raspberry Pi that is beautiful, robust, and extensible that takes care of all the heavy lifting.
### Helpful Links
* [Pre-proposal](https://docs.google.com/document/d/1ROzNNcilWC7lHQJEZ03QgzLXuZMqiJ-syehpt0rq6c0/edit?usp=sharing)
* [Proposal](https://docs.google.com/document/d/1C-n8nFJpg370SSToq7frqp2j0IJnLwnb9suswY5O0XM/edit?usp=sharing)
* [Design Proposal](https://docs.google.com/document/d/1xzOEgj76CU5GSXktFMSLb9811bB8YOqEsltoPMcFQvo/edit?usp=sharing)
* [Writeup](https://docs.google.com/document/d/1MWgH5I4wqVJmgLmTw7QpF-DdKZIoif703Zb8xldaZd8/edit?usp=sharing)
* [Poster](https://docs.google.com/a/uw.edu/presentation/d/1QKZU4JQztSeu26JYXcOtWvOXLwfvnrU9pZT7CIH-M4w/edit?usp=sharing)
* [Screencast (TBLINKED)](https://drive.google.com/file/d/0BwWXXKxOmqZHSUc3MWY3WFFSbE0/view?usp=sharing)
### Tutorial
#### Working with Streams
To start off let's create a Raspberry Pi agnostic stream to help us understand how the Stream system is set up and how we can use it to its full capacity. Let's consider a situation in which we have a list of names we want to go through and we want to make the names all caps, and then print all the unique names that start with E.

    names = ['Emerson', 'Spencer', 'Jake', 'Sally', 'Mattie', 'Emily', 'Emily']
    gen_stream = GeneratorStream(iter(names))

The second line can be disambiguated in this way. Let's consider the fact that we want to use an iterable `names` and move through it and do things at each point. Therefore, we want to pass in the iterator over these list of names by using `iter(names)` and the GeneratorStream will say,"Hey! Okay, that makes sense. I'll go through this for you!" Now, let's deal with transforming our data. We want to create a pipe in which our data goes through that meets all of these requirements that we want to print out as. Let's first take making the names uppercase, that's pretty easy.

    gen_stream.map(lambda x: x.upper())

We use the `.map()` function on our generator stream. The map function applies the function we give it to every value that passes through it. For each value we make it upper case. One thing to notice, also, is that `stream.map()` gives us a new Stream! Therefore we can do something pretty neat by chaining all of this together to achieve what we want in a nice way.

    gen_stream.map(lambda x: x.upper()).filter(lambda x: x.startswith('E'))

That's pretty clean right! We have another very similar function like earlier: `filter` in which we can pass in a function that evaluates to a truth value that we can use to determine if the item should pass through the stream. We only need to make sure these names are unique now. That's pretty easy with a single baked in function!

    gen_stream
        .map(lambda x: x.upper())
        .filter(lambda x: x.startswith('E'))
        .unique()

There we go! Mission accomplished. Now, let's tell this newest outer stream to do something when it gets a value: print. That's easy! One more line.

    gen_stream
        .map(lambda x: x.upper())
        .filter(lambda x: x.startswith('E'))
        .unique()
        .subscribe(print)

Now, let's work with a Raspberry Pi example.
#### Creating your own Streams
Now that we understand how streams can be used to manipulate data and respond to events, we'll look at how to use the system level streams provided to interface the Raspberry Pi's GPIO capabilities as well as network capabilities. There are an endless amount of systems that can be built by interfacing sensors with the Raspberry Pi and communicating with a web endpoint. For this example, consider a temperature sensor connected to the Raspberry Pi. We want to build an application that reads the input from the temperature sensor and sends a request to an HTTP endpoint when the temperature exceeds a certain threshold. We begin by creating a GPIOInputStream that corresponds to the gpio pin on the RPi that we have connected the sensor to (Let's say it's 6).

    temp_in = GPIOInputStream(6, 10)

Now we've created a GPIO input stream that will read the value of the temperature sensor connected to pin 6 every 10 seconds. In order to apply a threshold to the temperature, we filter the GPIOInputStream.

    temp_in
        .filter(lambda x: x > 60)

Next, we map each of these values to HTTP parameters that we can use to send a request to some endpoint.

    temp_in
        .filter(lambda x: x > 60)
        .map(lambda x: {
            'url': 'http://www.my.fun.endpoint.io/temp/?temp=' + x,
            'headers': {'Accept': 'text/plain'},
        })

Now we are ready to connect the result of this pipeline to an HTTPStream. We instantiate an HTTPStream object and specify the HTTP method when we create it. We connect the output of our pipeline to the input of our HTTP stream using the 'then' method on our pipeline.

    temp_in
        .filter(lambda x: x > 60)
        .map(lambda x: {
            'url': 'http://www.my.fun.endpoint.io/temp/?temp=' + x,
            'headers': {'Accept': 'text/plain'},
        }).then(HTTPStream(method='GET'))

And our pipeline is complete! We simply need to call the 'run()' method on the GPIOInputStream, and it will begin periodically reading values and propogating them down the pipeline. But what if we wish to do more than just pipe values directly to an endpoint? Say we'd like to store all of the temperature values in a file for logging purposes. We simply subscribe a FileOutputStream to same GPIOInputStream used above!

    temp_in.subscribe(FileOutputStream('path/to/file'))

One of the more useful aspects of the library is the ability to encapsulate the process of building a stream pipeline (like we did in this example) into a method. This concept of composition allows the design of complex streams using relatively simple building blocks. In order to generalize the pipeline above into a new stream, let's call it a HitEndpointOnGPIOThreshold stream (I know, it's a mouthful), we generalize the hard coded values to parameters and then enclose the entire composition in a function.

    def HitEndpointOnGPIOThreshold(gpio, frequency, threshold, endpoint):
        temp_in = GPIOInputStream(gpio, frequency)
        temp_in
            .filter(lambda x: x > 60)
            .map(lambda x: {
                'url': 'http://www.my.fun.endpoint.io/temp/?temp=' + x,
                'headers': {'Accept': 'text/plain'},
            }).then(HTTPStream(method='GET'))
        return temp_in

Now the user is free to start or stop this pipeline at their discretion. As each GPIOInputStream is running in its own thread, many instances of this pipeline can run concurrently, without the need for complex control structures and large loops.

### Examples
    names = ['Emerson', 'Spencer']

    # Make a Twilio stream with our credentials
    text_stream = TwilioStream(
        TWILIO_NUMBER,
        RECV_NUMBER,
        ACCOUNT_SID,
        AUTH_TOKEN,
    )

    # Make a generator stream over the names to work with
    gen_stream = GeneratorStream(iter(names))

    # Make a HTTP stream to work with the API we want to grab stuff from
    nice_api = HTTPStream(method='GET')

    # We take the names from the generator, create the format HTTPStream likes
    #   to pass in optional parameters, then we just funnel it to our Twilio
    #   Stream!
    gen_stream.map(
        lambda x: {
            'url': 'http://www.foaas.com/awesome/' + x,
            'headers': {'Accept': 'text/plain'},
        }
    ).then(nice_api).then(text_stream)

    # We flush the generator stream to get things started!
    gen_stream.flush()


  More examples can be found in abstract.py and a good agnostic example is
  the chat demo. Taking a look at pis.py can have the other half of the
  chatting demo. HourStream is also an example of easy usage of subclassing
  to accomplish a particular goal. The HourStream can then be subclassed
  further or used as a simple "in-between" Stream.

  All of our code should have sufficient documentation/naming schemes to
  make sense of further, extended development.
