import time

from cambrian.external import NetworkClientStream
from cambrian.external import SeparatorNetworkServerStream
from cambrian.functional import PeriodicGeneratorStream
from cambrian.system import GPIOOutputStream

# Identical to the server in abstract.py, but modified to
# use LED's to indicate the status of the chatroom
def chat_server_led():
    # Let's define two functions that are related to our server

    # We want to define a function that collects bytes together for the
    #   purpose of a stream later.
    def bytes_collect(x, y):
        if not x:
            return y
        return x + y

    # We want to create a function that decides whether a byte message contains
    #   the protocol separator.
    def protocol_fn(x):
        return '\n\n' in x.decode('ascii')

    # Let's make our server generator. This stream creates streams of clients.
    server_generator = SeparatorNetworkServerStream(None, 42069)

    # For funsies, we will put the clients into a list.
    clients = []
    
    rate_out = GPIOOutputStream(26)

    # Let's define a function to handle a new client stream.
    def _server_fn(new_client_stream):
        client_name = None

        # Let's define a function that sets the client name for the given
        #   stream.
        def _set_client_name(name):
            nonlocal client_name

            print(name, 'has joined the chat')

            client_name = name
            clients.append(client_name)

        # Let's define a function that makes the message really neat for
        #   the purposes of display.
        def _format_message(message):
            compiled_message = (
                (client_name + ': ').encode('ascii')
                + message
                + '\n'.encode('ascii')
            )
            print('Sending:', compiled_message)

            return compiled_message

        # Let's make the message baseline so it can be used for multiple
        #   purposes.
        def _baseline_message(message):
            return message.strip()

        # We need to deal with getting the name from the client.

        # We get the first thing from the client
        new_client_stream.first().map(
            lambda x: x.decode('ascii').strip()  # We then clean up the name
        ).subscribe(_set_client_name)  # We then subscribe the function we made

        # We need to deal with general message sfrom the client

        # We should skip the first message from the stream since the
        #   client will have their name in that message.
        baseline_user_messages_stream = new_client_stream.skip_n(
            1
        ).collect(  # We collect on bytes until we get the delimiter message
            bytes_collect,
            after_fn=protocol_fn,
        ).map(_baseline_message)  # We then map it to a baseline message

        # baseline_user_messages_stream.then(GPIOOutputLight)

        # We should now deal with messages to send to everyone, and we will
        #   format all messages then send it to the general server.
        baseline_user_messages_stream.map(
            lambda x: _format_message(x)
        ).then(server_generator)

        # We should now deal with extra commands. We filter messages to get
        #   the command we're making for.
        baseline_user_messages_stream.filter(
            lambda x: '!who' == x.decode('ascii')
        ).map(  # We map to a messsage to send to everyone and send it.
            lambda x:
            ('SERVER: Chatter(s): ' + str(clients) + '\n').encode('ascii')
        ).then(server_generator)

    # We subscribe our server handler and then start up the server.
    server_generator.subscribe(_server_fn)

    # These yellow LED's will represent connections to the server
    # These red LED's will visualize data flowing through the server
    rate_leds = map(lambda gpio: GPIOOutputStream(gpio), [26, 19, 13])
    # First three connections turn on LED
    server_generator.run()


def main():
    # Tumbleweed #
    chat_server_led()

if __name__ == '__main__':
    main()
