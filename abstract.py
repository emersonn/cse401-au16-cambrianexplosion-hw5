"""
Abstract Cambrian Demos
Spencer Anderson, Emerson Matson

Demos that are non Rasperry Pi
"""

import time

from twilio.rest import TwilioRestClient

# from cambrian.external import NetworkServerStream
from cambrian.external import SeparatorNetworkServerStream

from cambrian.streams import Stream

from cambrian.functional import GeneratorStream
from cambrian.functional import PeriodicGeneratorStream

# from cambrian.system import HTTPStream

from secret import AUTH_TOKEN

TWILIO_NUMBER = '+12532011007'
RECV_NUMBER = '+12532799621'
ACCOUNT_SID = 'ACec42369eff9e0db6a8dee3330a51fa1f'


def easy_generator_demo():
    print(
        'I am going to print out every number from 1 to 10 every .1 seconds.'
    )

    gen_stream = PeriodicGeneratorStream(iter(range(1, 11)), .1)
    gen_stream.subscribe(print)

    gen_stream.run()
    gen_stream.join()


def modified_generator_demo():
    print(
        'I am going to print out every power of 2'
        + ' with 4 as the ones digit until 2^15 every .1 seconds'
    )

    gen_stream = PeriodicGeneratorStream(iter(range(1, 16)), .1)
    gen_stream.map(
        lambda x: 2 ** x
    ).filter(
        lambda x: x % 10 == 4
    ).subscribe(print)

    gen_stream.run()
    gen_stream.join()


def nice_message_demo():
    print('I am going to print nice messages from a cool API from 4 people.')

    names = ['Billy', 'Joel,', 'Katy', 'Perry']

    gen_stream = GeneratorStream(iter(names))
    nice_api = HTTPStream(method='GET')

    gen_stream.map(
        lambda x: {
            'url': 'http://www.foaas.com/awesome/' + x,
            'headers': {'Accept': 'text/plain'},
        }
    ).then(nice_api)

    nice_api.subscribe(print)

    gen_stream.flush()


class TwilioStream(Stream):
    def __init__(
        self,
        twilio_number,
        receiver_number,
        account_sid,
        auth_token,
    ):
        super().__init__()

        self.twilio_number = twilio_number
        self.recv_number = receiver_number

        self.client = TwilioRestClient(account_sid, auth_token)

    def _push(self, message):
        self.client.messages.create(
            body=message,
            to=self.recv_number,
            from_=self.twilio_number,
        )


def twilio_demo():
    print('Let us send some very nice messages to someone.')

    names = ['Emerson', 'Spencer']

    # Make a Twilio stream with our credentials
    text_stream = TwilioStream(
        TWILIO_NUMBER,
        RECV_NUMBER,
        ACCOUNT_SID,
        AUTH_TOKEN,
    )

    # Make a generator stream over the names to work with
    gen_stream = GeneratorStream(iter(names))

    # Make a HTTP stream to work with the API we want to grab stuff from
    nice_api = HTTPStream(method='GET')

    # We take the names from the generator, create the format HTTPStream likes
    #   to pass in optional parameters, then we just funnel it to our Twilio
    #   Stream!
    gen_stream.map(
        lambda x: {
            'url': 'http://www.foaas.com/awesome/' + x,
            'headers': {'Accept': 'text/plain'},
        }
    ).then(nice_api).then(text_stream)

    # We flush the generator stream to get things started!
    gen_stream.flush()


def echo_relay_server_demo():
    # We make a network server stream that gives us all data sent
    server_stream = NetworkServerStream(None, 42069)

    # We reroute the server's data sent to itself
    server_stream.then(server_stream)


def chat_server_demo():
    # Let's define two functions that are related to our server

    # We want to define a function that collects bytes together for the
    #   purpose of a stream later.
    def bytes_collect(x, y):
        if not x:
            return y
        return x + y

    # We want to create a function that decides whether a byte message contains
    #   the protocol separator.
    def protocol_fn(x):
        return '\n\n' in x.decode('ascii')

    # Let's make our server generator. This stream creates streams of clients.
    server_generator = SeparatorNetworkServerStream(None, 42069)

    # For funsies, we will put the clients into a list.
    clients = []

    # Let's define a function to handle a new client stream.
    def _server_fn(new_client_stream):
        client_name = None

        # Let's define a function that sets the client name for the given
        #   stream.
        def _set_client_name(name):
            nonlocal client_name

            print(name, 'has joined the chat')

            client_name = name
            clients.append(client_name)

        # Let's define a function that makes the message really neat for
        #   the purposes of display.
        def _format_message(message):
            compiled_message = (
                (client_name + ': ').encode('ascii')
                + message
                + '\n'.encode('ascii')
            )
            print('Sending:', compiled_message)

            return compiled_message

        # Let's make the message baseline so it can be used for multiple
        #   purposes.
        def _baseline_message(message):
            return message.strip()

        # We need to deal with getting the name from the client.

        # We get the first thing from the client
        new_client_stream.first().map(
            lambda x: x.decode('ascii').strip()  # We then clean up the name
        ).subscribe(_set_client_name)  # We then subscribe the function we made

        # We need to deal with general message sfrom the client

        # We should skip the first message from the stream since the
        #   client will have their name in that message.
        baseline_user_messages_stream = new_client_stream.skip_n(
            1
        ).collect(  # We collect on bytes until we get the delimiter message
            bytes_collect,
            after_fn=protocol_fn,
        ).map(_baseline_message)  # We then map it to a baseline message

        # baseline_user_messages_stream.then(GPIOOutputLight)
        # User messages control the GPIO?

        # We should now deal with messages to send to everyone, and we will
        #   format all messages then send it to the general server.
        baseline_user_messages_stream.map(
            lambda x: _format_message(x)
        ).then(server_generator)

        # We should now deal with extra commands. We filter messages to get
        #   the command we're making for.
        baseline_user_messages_stream.filter(
            lambda x: '!who' == x.decode('ascii')
        ).map(  # We map to a messsage to send to everyone and send it.
            lambda x:
            ('SERVER: Chatter(s): ' + str(clients) + '\n').encode('ascii')
        ).then(server_generator)

    # We subscribe our server handler and then start up the server.
    server_generator.subscribe(_server_fn)
    server_generator.run()


def switch_text():
    text_stream = TwilioStream(
        TWILIO_NUMBER,
        RECV_NUMBER,
        ACCOUNT_SID,
        AUTH_TOKEN,
    )

    switch_stream = GPIOEventStream(21)
    switch_stream.map(lambda x: "The switch is " + str(x)).then(text_stream)
    switch_stream.run()


def chat_bots():
    # Assumes a server with a similar protocol as the one in the
    # chat server above is running
    MESSAGE_DELAY = 6

    # Creates a client stream that connects to a server and
    #   sends a message from an array at given intervals
    def CreateChatBot(messages, frequency):
        # REMEBER TO UPDATE THE ADDRESS FOR THE DEMO

        # Make a NetworkClientStream to connect to the server
        client_stream = NetworkClientStream("192.168.1.251", 42069)

        # Make a periodic generator stream to
        #   send messages at a particular rate
        input_stream = PeriodicGeneratorStream(iter(messages), frequency)

        # Make sure to follow the protocol with the messages and then send them
        input_stream.map(
            lambda x: (x + '\n\n').encode('ascii')
        ).then(client_stream)
        return input_stream

    # Set up the message we're going to send
    messagesSpencer = ["Spencer", "Hey Emerson", "I'm doing well, you?", "Yeah this class was the best", "Ras! You're a fan of streams too?", "Hip! Hip!"]
    messagesEmerson = ["Emerson", "Howdy partner, how are you?", "Pretty good, I love CSE401", "Word, I love streams", "What a neat surprise!", "Hurray!"]
    messagesRas = ["Prof. Ras", "Did somebody say streams?", "I've loved streams since my childhood!", "Great job boys! Enjoy the break!"]

    # Create our chat bots
    bot_spencer = CreateChatBot(messagesSpencer, MESSAGE_DELAY)
    bot_emerson = CreateChatBot(messagesEmerson, MESSAGE_DELAY)
    bot_ras = CreateChatBot(messagesRas, MESSAGE_DELAY)

    # Start each bot after a bit
    bot_spencer.run()
    time.sleep(MESSAGE_DELAY / 3)

    bot_emerson.run()
    time.sleep(MESSAGE_DELAY / 3 + MESSAGE_DELAY * 2)

    bot_ras.run()


def main():
    # TODO(emersonn): Maybe need to add optimization?
    #   Add an ifelse transformation?
    #   Open up endpoints to the Raspberry Pi to IFTTT?
    #   Make a Twilio example by creating a Stream to a Twilio API?
    #   Routing voice from Twilio to the Raspberry Pi
    #   Cool combining of synchronous streams?
    #   Make it easier to debug, especially in concurrent streams? Such as a
    #       debug flag in which can be set to print out pushed values, etc.
    #       Cool graphic that shows the streams you have made?
    #   Networks Streams that open up sockets and start reading from them
    #       or writes to them.
    #       Make an example of a echo server! IT'S LIKE ONE LINE!
    #   Show more examples of functional network streams and using collect.
    #   .collect() until a function says yes you can continue with that,
    #       or until a certain number of items have been pushed.

    # 3 demos
    #   Chat room
    #   Twilio button to HTTP to text? (switch to nice text demo function)
    #   Twilio switch text

    # Write examples of extensions and subclassing, most importantly such
    #   as switch streams and etc and show how easy it is to do so, because
    #   that was also a huge part of the language, such as HourStream.

    # Can do a throttle or a latest for the server blinking light?

    # easy_generator_demo()
    # modified_generator_demo()

    # nice_message_demo()

    # twilio_demo()
    # switch_text()

    # echo_relay_server_demo()
    chat_server_demo()

    # IFTTT demo is if switch is on what does the sensor say and send an email

if __name__ == '__main__':
    main()
