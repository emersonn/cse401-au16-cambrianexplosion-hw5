"""
Cambrian Streams
Spencer Anderson, Emerson Matson

Utility functions for the Cambrian library
"""

import socket


def get_socket(host, port, protocol, local=False):
    result_socket = None
    for result in socket.getaddrinfo(
        host,
        port,
        socket.AF_UNSPEC,
        protocol,
        0,
        socket.AI_PASSIVE,
    ):
        af, socktype, proto, canonname, sa = result
        try:
            result_socket = socket.socket(af, socktype, proto)
        except OSError as msg:
            print("Got an error:", msg)

            result_socket = None
            continue

        try:
            if local:
                # TODO(emersonn): Does this solve error of already bound?
                result_socket.setsockopt(
                    socket.SOL_SOCKET,
                    socket.SO_REUSEADDR,
                    1,
                )

                result_socket.bind(sa)
            else:
                result_socket.connect(sa)
        except OSError as msg:
            print("Got an error:", msg)

            result_socket.close()
            result_socket = None
            continue

        break

    return result_socket
