"""
Cambrian Streams
Spencer Anderson, Emerson Matson

Higher level file streams used for the Cambrian library
"""

import selectors
import socket
import threading

from cambrian.functional import PeriodicStream
from cambrian.streams import OutputStream
from cambrian.streams import Stream

from cambrian.utils import get_socket

"""Constants"""
MAX_BYTE_SIZE = 2048
MAX_LISTENERS = 15


class PeriodicFileInputStream(PeriodicStream):
    """Reads from a file periodically and pushes all the file's data"""

    def __init__(self, file_path, frequency):
        super().__init__(frequency)

        self.file_path = file_path

    def _update_on_time(self):
        # TODO(emersonn): Other options for read?
        with open(self.file_path, 'r') as f:
            self._push(f.read())


class FileOutputStream(OutputStream):
    """Outputs all data pushed to the stream into the file by overwriting."""

    def __init__(self, file_path):
        self.file_path = file_path

    def _push(self, value):
        # TODO(emersonn): Other options for read?
        with open(self.file_path, 'w') as f:
            f.write(value)


class NetworkClientStream(OutputStream):
    """Opens up a client stream to a given host and port

    Usage Information:
        All values pushed to this Stream will be broadcasted to the server.
        All values received by this Stream will be pushed to subscribers.
    """

    def __init__(self, host, port):
        super().__init__()

        self.host = host
        self.port = port

        self.socket = None
        while not self.socket:
            self.socket = get_socket(
                host,
                port,
                socket.SOCK_STREAM,
                local=False,
            )

        self.keep_running = True

        self.thread = threading.Thread(target=self._on_receive)
        self.thread.start()

    def _push(self, value):
        self.socket.sendall(value)

    def _push_to_callbacks(self, value):
        for callback in self.callbacks:
            callback(value)

    def _on_receive(self):
        while self.keep_running:
            self._push_to_callbacks(self.socket.recv(MAX_BYTE_SIZE))

    def stop(self):
        self.keep_running = False


class ConglomerateNetworkServerStream(Stream):
    """Opens up a server with the given host and port to relay all data

    Usage Information:
        Opens up a server with the given host and port which pushes all data
            into a single stream and pushes out all data received by clients
        Use None or '' for 'host' to get the general local server
    """

    def __init__(self, host, port, make_conn_tuple=False):
        """Initializes the Stream

        Args:
            make_conn_tuple (boolean): Makes a tuple of the message and
                client connection with the message first when data is
                pushed through to subscribers.
        """

        super().__init__()

        self.host = host
        self.port = port

        self.make_conn_tuple = make_conn_tuple

        self.selector = selectors.DefaultSelector()

        self.socket = None
        while not self.socket:
            self.socket = get_socket(
                host,
                port,
                socket.SOCK_STREAM,
                local=True,
            )

        self.socket.listen(MAX_LISTENERS)
        self.socket.setblocking(False)

        self.selector.register(
            self.socket,
            selectors.EVENT_READ,
            self._accept_connection,
        )

        self.keep_running = True

        self.clients = []

        self.thread = threading.Thread(target=self._event_loop)
        self.thread.start()

    def _accept_connection(self, sock, _):
        connection, address = self.socket.accept()
        connection.setblocking(False)

        self.clients.append(connection)

        self.selector.register(
            connection,
            selectors.EVENT_READ,
            self._on_receive,
        )

    def _push(self, value):
        destinations = self.clients

        if (
            isinstance(value, tuple)
            and isinstance(value[1], list)
            and set(value[1]).issubset(set(self.clients))
        ):
            message, val_destinations = value

            value = message
            destinations = val_destinations

        for client in destinations:
            client.sendall(value)

    def _push_to_callbacks(self, value):
        for callback in self.callbacks:
            callback(value)

    def _on_receive(self, connection, _):
        data = connection.recv(MAX_BYTE_SIZE)
        if data:
            if self.make_conn_tuple:
                data = (data, connection)
            self._push_to_callbacks(data)
        else:
            self.selector.unregister(connection)
            connection.close()

            self.clients.remove(connection)

    # TODO(emersonn): Add graceful finishing of the stream and its threads
    def _event_loop(self):
        while self.keep_running:
            events = self.selector.select()
            for key, mask in events:
                callback = key.data
                callback(key.fileobj, mask)


class _ClientStream(Stream):
    """Internal Client Stream for servers to be used by clients"""

    def __init__(self, connection, address):
        super().__init__()

        self.connection = connection
        self.address = address

    def _push(self, value):
        self.connection.sendall(value)

    def _push_to_callbacks(self, value):
        for callback in self.callbacks:
            callback(value)

    def _update(self):
        self._push_to_callbacks(self.connection.recv(MAX_BYTE_SIZE))

    def stop(self):
        super().stop()

        self.connection.close()


class SeparatorNetworkServerStream(Stream):
    """Server that pushes through Streams for new connected clients

    Usage Information:
        Opens up a server with the given host and port which pushes all data
            into a single stream and pushes out Streams that represent clients
    """

    def __init__(self, host, port):
        super().__init__()

        self.host = host
        self.port = port

        self.socket = None
        while not self.socket:
            self.socket = get_socket(
                host,
                port,
                socket.SOCK_STREAM,
                local=True,
            )

        self.socket.listen(MAX_LISTENERS)

        self.clients = []

    def _push(self, value):
        destinations = self.clients

        if (
            isinstance(value, tuple)
            and isinstance(value[1], list)
            and set(value[1]).issubset(set(self.clients))
        ):
            message, val_destinations = value

            value = message
            destinations = val_destinations

        for client in destinations:
            client._push(value)

    def _push_to_callbacks(self, value):
        for callback in self.callbacks:
            callback(value)

    def _update(self):
        connection, address = self.socket.accept()

        new_client = _ClientStream(connection, address)
        new_client.run()

        self.clients.append(new_client)

        self._push_to_callbacks(new_client)

    def stop(self):
        super().stop()

        # TODO(emersonn): Need to properly fix the socket accept connection
        for client in self.clients:
            client.stop()

        self.socket.close()
