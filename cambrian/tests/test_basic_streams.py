from cambrian.functional import PeriodicStream
from cambrian.streams import Stream

import time

from unittest import TestCase


class TestBasicStream(TestCase):
    def test_easy_push(self):
        found_values = []

        new_stream = Stream()
        new_stream.subscribe(lambda x: found_values.append(x))
        new_stream._push(1)

        self.assertEqual(found_values, [1])

    def test_multi_push(self):
        found_values = []

        new_stream = Stream()
        new_stream.subscribe(lambda x: found_values.append(x))
        new_stream._push_many([1, 2, 3])

        self.assertEqual(found_values, [1, 2, 3])

    def test_multi_push_mixed(self):
        found_values = []

        new_stream = Stream()
        new_stream.subscribe(lambda x: found_values.append(x))
        new_stream._push_many([1, 2, 3])
        new_stream._push(4)
        new_stream._push_many([5])

        self.assertEqual(found_values, [1, 2, 3, 4, 5])

    def test_easy_map(self):
        found_values = []

        new_stream = Stream()
        (
            new_stream
            .map(lambda x: x + 1)
            .subscribe(lambda x: found_values.append(x))
        )

        new_stream._push_many([1, 4])

        self.assertEqual(found_values, [2, 5])

    def test_easy_filter(self):
        found_values = []

        new_stream = Stream()
        (
            new_stream
            .filter(lambda x: x > 1)
            .subscribe(lambda x: found_values.append(x))
        )

        new_stream._push_many([1, 4])

        self.assertEqual(found_values, [4])

    def test_easy_unique(self):
        found_values = []

        new_stream = Stream()
        (
            new_stream
            .unique(lambda x: x)
            .subscribe(lambda x: found_values.append(x))
        )

        new_stream._push_many([1, 1, 2, 4, 2])

        self.assertEqual(found_values, [1, 2, 4])

    def test_easy_strings_unique(self):
        found_values = []

        new_stream = Stream()
        (
            new_stream
            .unique(lambda x: x)
            .subscribe(lambda x: found_values.append(x))
        )

        new_stream._push_many(['cat', 'dog', 'cat', 'dog', 'dog', 'meow'])

        self.assertEqual(found_values, ['cat', 'dog', 'meow'])

    def test_easy_throttle(self):
        found_values = []

        new_stream = Stream()
        (
            new_stream
            .throttle(100)
            .subscribe(lambda x: found_values.append(x))
        )

        new_stream._push_many(['cat', 'dog', 'cat', 'dog', 'dog', 'meow'])

        self.assertEqual(found_values, ['cat'])

    def test_double_val_throttle(self):
        found_values = []

        new_stream = Stream()
        (
            new_stream
            .throttle(0.1)
            .subscribe(lambda x: found_values.append(x))
        )

        new_stream._push('cat')
        time.sleep(1)
        new_stream._push('dog')

        self.assertEqual(found_values, ['cat', 'dog'])

    def test_zip_streams(self):
        found_values = []

        new_stream = Stream()
        new_other_stream = Stream()

        new_stream.zip(
            new_other_stream,
            lambda x, y: (x, y)
        ).subscribe(lambda x: found_values.append(x))

        new_stream._push(1)
        new_stream._push(2)
        new_other_stream._push(4)
        new_other_stream._push(5)
        new_stream._push(4)

        self.assertEqual(found_values, [(2, 4), (2, 5), (4, 5)])

    def _collect_fn(self, x, y):
        if not x:
            return y
        return x + y

    def test_easy_collect_num(self):
        found_values = []

        new_stream = Stream()

        new_stream.collect(self._collect_fn, after_num=3).subscribe(
            lambda x: found_values.append(x)
        )

        new_stream._push(1)
        new_stream._push(2)
        new_stream._push(2)

        self.assertEqual(found_values, [5])

    def test_more_collect_num(self):
        found_values = []

        new_stream = Stream()

        new_stream.collect(self._collect_fn, after_num=3).subscribe(
            lambda x: found_values.append(x)
        )

        new_stream._push(1)
        new_stream._push(2)
        new_stream._push(2)
        new_stream._push(4)

        self.assertEqual(found_values, [5])

    def test_multi_collect_num(self):
        found_values = []

        new_stream = Stream()

        new_stream.collect(self._collect_fn, after_num=3).subscribe(
            lambda x: found_values.append(x)
        )

        new_stream._push(1)
        new_stream._push(2)
        new_stream._push(2)
        new_stream._push(1)
        new_stream._push(1)
        new_stream._push(2)

        self.assertEqual(found_values, [5, 4])

    def _collect_fn_check(self, val):
        return val > 5

    def test_work_collect_fn(self):
        found_values = []

        new_stream = Stream()

        new_stream.collect(
            self._collect_fn,
            after_fn=self._collect_fn_check,
        ).subscribe(
            lambda x: found_values.append(x)
        )

        new_stream._push(1)
        new_stream._push(2)
        new_stream._push(2)

        self.assertEqual(found_values, [])

    def test_single_collect_fn(self):
        found_values = []

        new_stream = Stream()

        new_stream.collect(
            self._collect_fn,
            after_fn=self._collect_fn_check,
        ).subscribe(
            lambda x: found_values.append(x)
        )

        new_stream._push(1)
        new_stream._push(2)
        new_stream._push(2)
        new_stream._push(3)

        self.assertEqual(found_values, [8])

    def test_single_more_collect_fn(self):
        found_values = []

        new_stream = Stream()

        new_stream.collect(
            self._collect_fn,
            after_fn=self._collect_fn_check,
        ).subscribe(
            lambda x: found_values.append(x)
        )

        new_stream._push(1)
        new_stream._push(2)
        new_stream._push(2)
        new_stream._push(3)
        new_stream._push(2)

        self.assertEqual(found_values, [8])

    def test_multi_collect_fn(self):
        found_values = []

        new_stream = Stream()

        new_stream.collect(
            self._collect_fn,
            after_fn=self._collect_fn_check,
        ).subscribe(
            lambda x: found_values.append(x)
        )

        new_stream._push(1)
        new_stream._push(2)
        new_stream._push(2)
        new_stream._push(3)
        new_stream._push(3)
        new_stream._push(12)

        self.assertEqual(found_values, [8, 15])

    def test_first_fn(self):
        found_values = []

        new_stream = Stream()

        new_stream.first().subscribe(
            lambda x: found_values.append(x)
        )

        new_stream._push(1)
        new_stream._push(2)
        new_stream._push(2)
        new_stream._push(3)
        new_stream._push(3)
        new_stream._push(12)

        self.assertEqual(found_values, [1])

    def test_empty_first_fn(self):
        found_values = []

        new_stream = Stream()

        new_stream.first().subscribe(
            lambda x: found_values.append(x)
        )

        self.assertEqual(found_values, [])

    def test_skip_n_empty(self):
        found_values = []

        new_stream = Stream()

        new_stream.skip_n(3).subscribe(
            lambda x: found_values.append(x)
        )

        new_stream._push(1)
        new_stream._push(2)

        self.assertEqual(found_values, [])

    def test_skip_n_empty_border(self):
        found_values = []

        new_stream = Stream()

        new_stream.skip_n(3).subscribe(
            lambda x: found_values.append(x)
        )

        new_stream._push(1)
        new_stream._push(2)
        new_stream._push(3)

        self.assertEqual(found_values, [])

    def test_skip_n_non_empty(self):
        found_values = []

        new_stream = Stream()

        new_stream.skip_n(3).subscribe(
            lambda x: found_values.append(x)
        )

        new_stream._push(1)
        new_stream._push(2)
        new_stream._push(2)
        new_stream._push(3)
        new_stream._push(3)
        new_stream._push(12)

        self.assertEqual(found_values, [3, 3, 12])


class ExamplePeriodicStream(PeriodicStream):
    def __init__(self, frequency):
        super().__init__(frequency)

    def _update_on_time(self):
        self._push(1)


class TestPeriodicStream(TestCase):
    def test_easy_periodic(self):
        found_values = []

        example_stream = ExamplePeriodicStream(1)
        example_stream.subscribe(lambda x: found_values.append(x))
        example_stream.run()

        time.sleep(1.5)

        self.assertEqual(found_values, [1, 1])
        example_stream.stop()
