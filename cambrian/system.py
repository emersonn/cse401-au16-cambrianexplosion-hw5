"""
Low level streams
Spencer Anderson, Emerson Matson

Low level streams that connect directly to the Pi
"""

import atexit, requests

import RPi.GPIO as GPIO
from cambrian.streams import OutputStream, Stream
from cambrian.functional import PeriodicStream

# Outputs a gpio signal to the specified gpio pin
class GPIOOutputStream(OutputStream):
    def __init__(self, gpio):
        super().__init__()
        # TODO(): Error handling
        self.gpio = gpio
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(gpio, GPIO.OUT)
        # Clean up after ourselves
        atexit.register(lambda: GPIO.cleanup(gpio))

    def _push(self, value):
        if not value:
            GPIO.output(self.gpio, GPIO.LOW)
        elif value:
            GPIO.output(self.gpio, GPIO.HIGH)
        else:
            print("Invalid value: " + value + " output to GPIO " + self.gpio)

# Periodically polls a gpio for input
class GPIOInputStream(PeriodicStream):
    def __init__(self, gpio, frequency):
        super().__init__(frequency)

        self.gpio = gpio
        self.frequency = frequency

        GPIO.setmode(GPIO.BCM)
        GPIO.setup(gpio, GPIO.IN)

        # Clean up after ourselves
        atexit.register(lambda: GPIO.cleanup(gpio))

    def _update_on_time(self):
        # Read a value from the gpio input and push to subs
        self._push(GPIO.input(self.gpio))

# Uses hardware interrupts to detect signal changes
class GPIOEventStream(Stream):
    def __init__(self, gpio):
        super().__init()

        self.gpio = gpio
        GPIO.add_event_detect(gpio, GPIO.RISING, lambda: self._push(GPIO.HIGH))
        GPIO.add_event_detect(gpio, GPIO.FALLING, lambda: self._push(GPIO.LOW))

        atexit.register(lambda: GPIO.remove_event_detect(gpio))

    def _update(self):
        GPIO.wait_for_edge(self.gpio, GPIO.BOTH)

class HTTPStream(Stream):
    def __init__(self, url=None, method='GET'):
        super().__init__()
        self.url = url
        self.method = method

    def _push(self, args):
        if 'url' not in args:
            args['url'] = self.url

        if 'method' not in args:
            args['method'] = self.method

        # Make request and read response
        res = requests.request(**args)

        # Push response to subscribers
        super()._push(res.text)

# TODO(emersonn): Finish this!


class PeriodicHTTPInputStream(PeriodicStream):
    def __init__(self):
        pass

# A stream that interfaces the MCP3008 analogue to digital converter
# and reports a value from 0 to 1028 representing the analogue
# signal on the specified analogue input pin.
# frequency : sampling frequency in seconds
# ainput    : analogue input number on MCP3008
# clkpin    : clock pin
# cspin     : chip select pin
# dinpin    : digital input pin
# doutpin   : digital output pin
# See https://cdn-shop.adafruit.com/datasheets/MCP3008.pdf for
#   chip schematic
class MCP3008Stream(PeriodicStream):
    def __init__(self, frequency, ainput, clkpin, cspin, dinpin, doutpin):
        super().__init__(frequency)

        # Initialize pins
        GPIO.setup(clkpin, GPIO.OUT)
        GPIO.setup(cspin, GPIO.OUT)
        GPIO.setup(dinpin, GPIO.OUT)
        GPIO.setup(doutpin, GPIO.IN)


    def _update_on_time(self):
        ## Startup sequence
        # Bring CS and CLK pins low
        GPIO.output(cspin, GPIO.HIGH)
        GPIO.output(clkpin, GPIO.LOW)
        GPIO.output(cspin, GPIO.LOW)

        # Prepare the configuration bits for startup
        # and channel selection
        config = ainput
        config |= 0x18 # Leading 2 bits for startup
        config <<= 3
        for i in range(5):
            # Set data in pin to the 8th bit's value
            if (config & 0x80):
                GPIO.output(dinpin, GPIO.HIGH)
            else:
                GPIO.output(dinpin, GPIO.LOW)
            # Toggle the clock to push the value
            GPIO.output(clkpin, GPIO.HIGH)
            GPIO.output(clkpin, GPIO.LOW)
            # Move the next bit over
            config <<= 1

        # Read null bit then the next ten bits for result
        output = 0
        for i in range(12):
            # Toggle the clock to get the next bit
            GPIO.output(clkpin, GPIO.HIGH)
            GPIO.output(clkpin, GPIO.LOW)
            # Ignore the first null bit
            if (i > 1):
                output |= GPIO.input(doutpin)
                output <<= 1

        # Set chip select to low for next cycle
        GPIO.output(cspin, GPIO.HIGH)

        self._push(output)
