"""
Cambrian Streams
Spencer Anderson, Emerson Matson

Streams used for the Cambrian library
"""

import threading


class Stream(object):
    """The most basic Stream that provides basic functionality for Streams

    Notes:
        When subclassing Stream with a Stream that must run as a thread,
            use ._update() as the overridden method to keep calling.

    Example usage:
        s = Stream()

        (
            s
                .map(lambda x: x + 2)
                .filter(lambda x: x > 5)
                .subscribe(lambda x: print(x))
        )
    """

    def __init__(self):
        self.callbacks = []

    def _push(self, value):
        """Internal function used for pushing values to the stream"""

        for callback in self.callbacks:
            callback(value)

    def _push_many(self, new_values):
        """Internal function used for pushing many values to a given stream"""

        for value in new_values:
            self._push(value)

    def subscribe(self, callback):
        """Subscribes the given function to the stream for pushes

        Args:
            callback (function(value)): Function that takes the pushed value
                from the stream.
        """
        self.callbacks.append(callback)

    def map(self, map_fn):
        """Maps the output of pushes from the stream with the given function

        Args:
            callback (function(value): new_value): Function that takes a pushed
                value and maps it to the output by returning it.

        Returns:
            Stream: New mapped value stream.
        """

        new_stream = Stream()

        self.subscribe(lambda x: new_stream._push(map_fn(x)))

        return new_stream

    def first(self):
        """Takes the first value pushed through the stream"""

        new_stream = Stream()

        is_first = True

        def _first_subscription(x):
            nonlocal is_first

            if is_first:
                new_stream._push(x)
                is_first = False

        self.subscribe(_first_subscription)

        return new_stream

    def filter(self, filter_fn):
        """Filters the values of the streams with the given filter function"""

        new_stream = Stream()

        def _filter_subscription(x):
            if filter_fn(x):
                new_stream._push(x)

        self.subscribe(_filter_subscription)

        return new_stream

    def skip_n(self, n):
        """Skips the first n values that are pushed through the stream"""

        new_stream = Stream()

        current = 0

        def _skip_n_subscription(x):
            nonlocal current
            current += 1

            if current > n:
                new_stream._push(x)

        self.subscribe(_skip_n_subscription)

        return new_stream

    def throttle(self, throttle_delay):
        """Throttles by taking input every given number of seconds"""

        new_stream = Stream()
        throttled = False

        def _throttle_callback():
            nonlocal throttled
            throttled = False

        def _throttle_subscription(x):
            nonlocal throttled
            if not throttled:
                new_stream._push(x)
                throttled = True

                resetter = threading.Timer(throttle_delay, _throttle_callback)
                resetter.daemon = True

                resetter.start()

        self.subscribe(_throttle_subscription)

        return new_stream

    def unique(self, uniqueness_fn):
        """Makes the stream unique by only pushing decidedly unique values"""

        new_stream = Stream()
        previous_values = set()

        def _unique_subscription(x):
            unique_val = uniqueness_fn(x)
            if unique_val not in previous_values:
                previous_values.add(unique_val)
                new_stream._push(x)

        self.subscribe(_unique_subscription)

        return new_stream

    def zip(self, other_stream, value_fn):
        """Outputs values that are combinations of values from both streams"""

        new_stream = Stream()

        has_A_produced = False
        has_B_produced = False

        last_A_val = None
        last_B_val = None

        def _zip_subscription_A(x):
            nonlocal has_A_produced
            nonlocal last_A_val

            last_A_val = x
            has_A_produced = True

            if has_A_produced and has_B_produced:
                new_stream._push(value_fn(last_A_val, last_B_val))

        def _zip_subscription_B(x):
            nonlocal has_B_produced
            nonlocal last_B_val

            last_B_val = x
            has_B_produced = True

            if has_A_produced and has_B_produced:
                new_stream._push(value_fn(last_A_val, last_B_val))

        self.subscribe(_zip_subscription_A)
        other_stream.subscribe(_zip_subscription_B)

        return new_stream

    def collect(self, collect_fn, after_num=None, after_fn=None):
        """Collects the values with the collect function after the given time

        Notes:
            Collect function MUST handle the first argument being None
                as a signal for the first iteration
        """

        # TODO(emersonn): Can implement after_num with after_fn functions

        assert after_num or after_fn is not None
        assert not after_num or after_num > 0

        new_stream = Stream()

        collection = None
        current_num = 0

        def _collect_subscription(x):
            nonlocal collection
            nonlocal current_num

            current_num += 1

            collection = collect_fn(collection, x)

            if (
                after_num and current_num == after_num
                or after_fn is not None and after_fn(collection)
            ):
                new_stream._push(collection)
                collection = None

                if after_num and current_num == after_num:
                    current_num = 0

        self.subscribe(_collect_subscription)

        return new_stream

    def run(self):
        """Runs this Stream 'concurrently'"""

        self.thread = threading.Thread(target=self._run)
        self.continue_running = True

        self.thread.start()

    def run_for(self, seconds):
        """Runs this Stream 'concurrently' for the given seconds"""

        self.run()

        def stopper_fn():
            self.stop()

        stopper = threading.Timer(seconds, stopper_fn)
        stopper.daemon = True

        stopper.start()

    def join(self):
        """Helps block the main thread for 'concurrent' Streams"""

        while self.continue_running:
            pass

    def _run(self):
        """Internal function for handling running 'concurrent' Streams"""

        while self.continue_running:
            self._update()

    def _update(self):
        """Implement this method for Streams that depend on intervals

        Example usage:
            (every ten seconds):
                self._push(new_value)
        """

        pass

    def stop(self):
        """Finishes the given 'concurrent' Stream"""

        # TODO(emersonn): Implement this
        assert self.thread is not None

        self.continue_running = False
        self.thread = None

    def then(self, output_stream):
        """Pushes all values of self into the output Stream"""

        # TODO(emersonn): Implement this and then IOStreams such as FileStreams
        # TODO(emersonn): Assert it is an output stream?
        # TODO(emersonn): Motion detector example?

        self.callbacks.append(output_stream._push)

        return output_stream


class OutputStream(object):
    """General output stream that allows pushing of values to abstract actions

    Required implemented methods:
        _push(new_value): A given value to push to the output.
    """

    pass
