"""
Functional Streams
Emerson Matson, Spencer Anderson

Functional multi-purpose streams
"""

import datetime
import time

from cambrian.streams import Stream


class PeriodicStream(Stream):
    """Periodic Stream in which pulls data at a given frequency

    Required implemented methods:
        _update_on_time(new_value): A given value to push to the output
            after a given frequency.
    """

    def __init__(self, frequency):
        """Initializes a Periodic Stream

        Args:
            frequency (int): Seconds to wait between updates
        """
        super().__init__()
        self.frequency = frequency

    def _update(self):
        """Internal update function"""

        self._update_on_time()
        time.sleep(self.frequency)


class SwitchStream(Stream):
    """Switch Stream in which pull data only when the switch is on"""

    def __init__(self, switch_fn, frequency):
        """Initializes a Switch Stream

        Args:
            frequency (int): Seconds to wait in between updates
            switch_fn (return: boolean): Function to call to determine whether
                to turn on the stream
        """
        super().__init__()

        self.frequency = frequency
        self.switch_fn = switch_fn

    def _update(self):
        """Internal update function"""

        if self.switch_fn():
            self._update_on_time()
        time.sleep(self.frequency)


class GeneratorStream(Stream):
    """Generator stream which produces new values when queried with generate"""

    # TODO(emersonn): Reformat this as a stream that takes a push of a value
    #   which in its case would be a generator?
    # Maybe take an iterable instead? Then call iter() on it?
    def __init__(self, generator):
        super().__init__()

        self.generator = generator

    def generate(self):
        self._push(next(self.generator))

    def flush(self):
        for item in self.generator:
            self._push(item)


class PeriodicGeneratorStream(PeriodicStream):
    """Pushes through the values from the generator at a given frequency"""

    def __init__(self, generator, frequency):
        super().__init__(frequency)

        self.gen_stream = GeneratorStream(generator)
        self.gen_stream.subscribe(lambda x: self._push(x))

    def _update_on_time(self):
        try:
            self.gen_stream.generate()
        except StopIteration:
            self.stop()


class SpreadStream(PeriodicStream):
    """Stream that spreads given values in push over time"""

    def __init__(self, interval):
        super().__init__(interval)

        self.vals = []

    def push(self, value):
        self.vals.append(value)

    def _update_on_time(self, new_value):
        if self.vals:
            self._push(self.vals.pop())
